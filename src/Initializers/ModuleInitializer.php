<?php

namespace VmdCms\Modules\Reviews\Initializers;;

use VmdCms\CoreCms\Initializers\AbstractModuleInitializer;

class ModuleInitializer extends AbstractModuleInitializer
{
    const SLUG = 'reviews';
    const ALIAS = 'Reviews';

    public function __construct()
    {
        parent::__construct();
        $this->stubBuilder->setPublishServices(true);
        $this->stubBuilder->setPublishDTO(true);
        $this->stubBuilder->setPublishEntity(true);
    }

    /**
     * @inheritDoc
     */
    public static function moduleAlias(): string
    {
        return self::ALIAS;
    }

    /**
     * @inheritDoc
     */
    public static function moduleSlug(): string
    {
        return self::SLUG;
    }

    public function seedCoreEvents()
    {
    }
}
