<?php

namespace VmdCms\Modules\Reviews\Sections;

use VmdCms\CoreCms\Contracts\Dashboard\Forms\FormInterface;
use VmdCms\CoreCms\CoreModules\CoreTranslates\Services\CoreLang;
use VmdCms\CoreCms\Facades\Column;
use VmdCms\CoreCms\Facades\ColumnEditable;
use VmdCms\CoreCms\Facades\Display;
use VmdCms\CoreCms\Facades\Form;
use VmdCms\CoreCms\Facades\FormComponent;
use VmdCms\CoreCms\Facades\FormTableColumn;
use VmdCms\CoreCms\Models\CmsSection;

class Review extends CmsSection
{
    /**
     * @var string
     */
    protected $slug = 'reviews';

    /**
     * @inheritDoc
     */
    public function getTitle() : string
    {
        return CoreLang::get('reviews');
    }

    public function display()
    {
        return Display::dataTable([
            Column::text('id','#'),
            Column::text('rate',CoreLang::get('rating')),
            ColumnEditable::switch('active')->sortable()->alignCenter(),
            Column::date('created_at'),
        ])->orderDefault(function ($query){
            $query->orderBy('created_at','desc');
        })->setSearchable(true)->orderable();
    }

    /**
     * @return FormInterface
     */
    public function create() : FormInterface
    {
        return $this->edit(null);
    }

    /**
     * @param int|null $id
     * @return FormInterface
     */
    public function edit(?int $id) : FormInterface
    {
        return Form::panel([
            FormComponent::radio('active'),
            FormComponent::input('rate',CoreLang::get('rating')),
            FormComponent::ckeditor('body',CoreLang::get('content')),
            FormComponent::datatable('images',CoreLang::get('images'))->setComponents([
                FormTableColumn::image('value',CoreLang::get('image'))->setHelpText('Рекомендуемые пропорции 10:9 (500px х 450px)'),
                FormTableColumn::input('title'),
                FormTableColumn::switch('active')->setDefault(true),
            ])->addClass('pt-5'),
            FormComponent::datatable('files',CoreLang::get('files'))->setComponents([
                FormTableColumn::image('value',CoreLang::get('file')),
                FormTableColumn::input('title'),
                FormTableColumn::switch('active'),
            ])->addClass('pt-10'),
        ]);
    }

    public function getCmsModelClass(): string
    {
        return \App\Modules\Reviews\Models\Review::class;
    }
}
