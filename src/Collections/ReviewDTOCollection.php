<?php

namespace VmdCms\Modules\Reviews\Collections;

use VmdCms\CoreCms\Collections\CoreCollectionAbstract;
use VmdCms\Modules\Reviews\Contracts\ReviewDTOCollectionInterface;
use VmdCms\Modules\Reviews\Contracts\ReviewDTOInterface;

class ReviewDTOCollection extends CoreCollectionAbstract implements ReviewDTOCollectionInterface
{
    /**
     * @param ReviewDTOInterface $dto
     */
    public function append(ReviewDTOInterface $dto)
    {
        $this->collection->add($dto);
    }
}
