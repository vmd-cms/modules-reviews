<?php

namespace VmdCms\Modules\Reviews\Contracts;

use Illuminate\Contracts\Support\Arrayable;
use VmdCms\Modules\Reviews\Models\Review;

interface ReviewDTOInterface extends Arrayable
{
    /**
     * ReviewDTOInterface constructor.
     * @param Review $model
     */
    public function __construct(Review $model);

    /**
     * @return int|null
     */
    public function getId(): ?int;

}
