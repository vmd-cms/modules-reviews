<?php

namespace VmdCms\Modules\Reviews\Contracts;

use VmdCms\CoreCms\Contracts\Collections\CollectionInterface;

interface ReviewDTOCollectionInterface extends CollectionInterface
{
    /**
     * @param ReviewDTOInterface $dto
     */
    public function append(ReviewDTOInterface $dto);

}
