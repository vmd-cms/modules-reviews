<?php

namespace VmdCms\Modules\Reviews\DTO;

use VmdCms\Modules\Reviews\Contracts\ReviewDTOInterface;
use VmdCms\Modules\Reviews\Models\Review;

class ReviewDTO implements ReviewDTOInterface
{
    protected $id;

    public function __construct(Review $model)
    {
        $this->id = $model->id ?? null;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }


    public function toArray()
    {
        return [
          'id' => $this->id
        ];
    }
}
