<?php

namespace VmdCms\Modules\Reviews\Models;

use Illuminate\Database\Eloquent\Builder;
use VmdCms\CoreCms\Contracts\Models\ActivableInterface;
use VmdCms\CoreCms\Contracts\Models\OrderableInterface;
use VmdCms\CoreCms\Contracts\Models\TreeableInterface;
use VmdCms\CoreCms\Exceptions\Modules\ModuleNotActiveException;
use VmdCms\CoreCms\Models\CmsModel;
use VmdCms\CoreCms\Traits\Models\Activable;
use VmdCms\CoreCms\Traits\Models\Orderable;
use VmdCms\CoreCms\Traits\Models\Treeable;
use VmdCms\Modules\Reviews\Models\Media\ReviewMediaFile;
use VmdCms\Modules\Reviews\Models\Media\ReviewMediaImage;

class Review extends CmsModel implements ActivableInterface, TreeableInterface, OrderableInterface
{
    use Activable, Treeable, Orderable;

    public static function table(): string
    {
       return 'reviews';
    }

    public static function getResourceModel(){
        return null;
    }

    public static function getModelKey(){
        return null;
    }

    protected static function boot()
    {
        parent::boot();

        if(static::getResourceModel()){
            static::addGlobalScope('resource', function (Builder $builder) {
                $builder->where('resource', static::getResourceModel());
            });
            static::creating(function (Review $model){
                if(!empty($model->order)) return;
                $lastItem = Review::where('resource',static::getResourceModel())->orderBy('order','desc')->first();
                if($lastItem instanceof Review){
                    $model->order = intval($lastItem->order) + 1;
                }
            });
            static::saving(function (Review $model){
                $model->resource = static::getResourceModel();
            });
        }

        if(static::getModelKey()){
            static::addGlobalScope('key', function (Builder $builder) {
                $builder->where('key', static::getModelKey());
            });
            static::saving(function (Review $model){
                $model->key = static::getModelKey();
            });
        }
    }

    public function author(){
        if(!class_exists(\VmdCms\Modules\Users\Models\User::class)){
            throw new ModuleNotActiveException();
        }
        return $this->belongsTo(\VmdCms\Modules\Users\Models\User::class,'author_id','id');
    }

    public function images()
    {
        return $this->hasMany(ReviewMediaImage::class,ReviewMediaImage::getForeignField(),'id')->orderBy('order');
    }

    public function files()
    {
        return $this->hasMany(ReviewMediaFile::class,ReviewMediaFile::getForeignField(),'id')->orderBy('order');
    }
}
