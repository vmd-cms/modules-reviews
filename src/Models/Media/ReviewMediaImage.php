<?php

namespace VmdCms\Modules\Reviews\Models\Media;

use VmdCms\CoreCms\Contracts\Models\HasMediaDimensionsInterface;
use VmdCms\CoreCms\Traits\Models\HasImagePath;
use VmdCms\CoreCms\Traits\Models\HasMediaDimensions;

class ReviewMediaImage extends ReviewMedia implements HasMediaDimensionsInterface
{
    use HasImagePath, HasMediaDimensions;

    public static function getModelKey()
    {
        return 'image';
    }

    /**
     * @return string|null
     */
    protected function getImageValue(): ?string
    {
        return $this->value ?? null;
    }
}
