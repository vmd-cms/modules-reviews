<?php

namespace VmdCms\Modules\Reviews\Models\Media;

use Illuminate\Database\Eloquent\Builder;
use VmdCms\CoreCms\Contracts\Models\CmsRelatedModelInterface;
use VmdCms\CoreCms\Models\CmsModel;
use VmdCms\Modules\Reviews\Models\Review;

class ReviewMedia extends CmsModel implements CmsRelatedModelInterface
{

    public static function table(): string
    {
        return 'reviews_media';
    }

    /**
     * @return string
     */
    public static function getForeignField() : string
    {
        return static::getBaseModelClass()::table() . '_id';
    }

    /**
     * @return string
     */
    public static function getBaseModelClass(): string
    {
        return Review::class;
    }

    public static function getModelKey(){
        return null;
    }

    protected static function boot()
    {
        parent::boot();

        if(static::getModelKey()){
            static::addGlobalScope('key', function (Builder $builder) {
                $builder->where('key', static::getModelKey());
            });
        }

        static::saving(function (ReviewMedia $model){
            $model->key = static::getModelKey();
        });
    }
}
