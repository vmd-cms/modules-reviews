<?php

namespace VmdCms\Modules\Reviews\Models\Media;

use VmdCms\CoreCms\Services\Files\StorageAdapter;

class ReviewMediaFile extends ReviewMedia
{
    public static function getModelKey()
    {
        return 'file';
    }

    /**
     * @return string
     */
    public function getFilePathAttribute()
    {
        return StorageAdapter::getInstance()->getStoragePath($this->value);
    }
}
