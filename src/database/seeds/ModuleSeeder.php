<?php
namespace VmdCms\Modules\Reviews\database\seeds;

use VmdCms\CoreCms\CoreModules\CoreTranslates\Services\CoreLang;
use VmdCms\CoreCms\Initializers\AbstractModuleSeeder;
use App\Modules\Reviews\Sections\Review;

class ModuleSeeder extends AbstractModuleSeeder
{
    /**
     * @inheritDoc
     */
    protected function getSectionsAssoc(): array
    {
        return [
            Review::class => CoreLang::get('reviews'),
        ];
    }

    /**
     * @inheritDoc
     */
    protected function getNavigationAssoc(): array
    {
        return [
            [
                "slug" => "shop_group",
                "title" => CoreLang::get('shop'),
                "is_group_title" => true,
                "order" => 2,
                "children" => [
                    [
                        "slug" => "reviews",
                        "title" => CoreLang::get('reviews'),
                        "section_class" => Review::class,
                        "icon" => "icon icon-shop",
                    ]
                ]
            ],
        ];
    }

}
