<?php

use VmdCms\Modules\Reviews\Models\Review as model;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(model::table(), function (Blueprint $table) {
            $table->increments(model::getPrimaryField());
            $table->string('key',32)->nullable();
            $table->string('resource',128)->nullable();
            $table->integer('resource_id')->unsigned()->nullable();
            $table->integer('parent_id')->unsigned()->nullable();
            $table->integer('author_id')->unsigned()->nullable();
            $table->integer('rate')->nullable();
            $table->text('body')->nullable();
            $table->text('data')->nullable();
            $table->boolean('active')->default(false);
            $table->boolean('display_flag')->default(false);
            $table->integer('order')->unsigned()->default(1);
            $table->timestamp('public_at')->nullable();
            $table->timestamps();
        });

        Schema::table(model::table(), function (Blueprint $table){
            $table->foreign('parent_id', model::table() . '_parent_id_fk')
                ->references('id')->on(model::table())
                ->onUpdate('CASCADE')->onDelete('SET NULL');
        });

        if(Schema::hasTable('users')){
            Schema::table(model::table(), function (Blueprint $table){
                $table->foreign('author_id', model::table() . '_author_id_fk')
                    ->references('id')->on('users')
                    ->onUpdate('CASCADE')->onDelete('SET NULL');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(model::table());
    }
}
